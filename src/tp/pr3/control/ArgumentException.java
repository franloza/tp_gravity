package tp.pr3.control;

/**
 * Exception generated when any execution argument is invalid.
 *
 * @author: Alvaro Bermejo
 * @author: Francisco Lozano
 * @version: 03/03/2015
 * @since: Assignment 3
 */

public class ArgumentException extends Exception {
	
	//Constructors
	
	/** No-argument constructor.*/
	public ArgumentException() {

	}

	/** Constructor with a detail-message parameter.*/
	public ArgumentException(String msg) {
		super(msg);
	}

	/** Constructor with a detail-message parameter and a cause parameter.*/
	public ArgumentException(String msg, Throwable cause) {
		super(msg);
		cause.printStackTrace();
	}

	/** Constructor with a cause parameter..*/
	public ArgumentException(Throwable cause) {
		cause.printStackTrace();
	}

}


