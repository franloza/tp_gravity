package tp.pr3;

import tp.pr3.control.ArgumentException;
import tp.pr3.control.Arguments;
import tp.pr3.control.Controller;
import tp.pr3.control.GameTypeFactory;
import tp.pr3.logic.Game;
import java.util.Scanner;

/**
 * Class that contains the entry point to the application.
 *
 * @author: Alvaro Bermejo
 * @author: Francisco Lozano
 * @version: 03/03/2015
 */

public class Main {

	//Methods	

	/**
	 * Main method of the application
	 *
	 * @param args Arguments passed to the application. Not used.
	 */
	public static void main(String[] args) {
		Arguments arguments;
		try {
			arguments = new Arguments(args);
			GameTypeFactory factory = arguments.getFactory();
			if (factory != null) {
				Game game = arguments.getGame();
				Scanner in = new Scanner(System.in);
				Controller control = new Controller(factory, game, in);
				control.run();
				System.exit(0);
			}
		} catch (ArgumentException e) {
			System.err.println("Incorrect use: " + e.getMessage());
			System.err.println("For more details, use -h|--help.");
			System.exit(1);
		}
	}
}
