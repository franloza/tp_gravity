package tp.pr3.logic;

/**
 * Represents the colour information of a counter.
 * The enumeration is used to store the information of each position on the board, for which reason it also contains a symbol to indicate the absence of a counter in that position.
 * It is also used to identify the player, for example for the information of whose turn it is.
 *
 * @author: Alvaro Bermejo
 * @author: Francisco Lozano
 * @version: 08/01/2015
 * @since: Assignment 1
 */

public enum Counter {
	EMPTY, WHITE, BLACK
}

