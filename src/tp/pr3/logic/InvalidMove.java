package tp.pr3.logic;

/**
 * Exception generated when an attempt is made to execute an invalid move.
 *
 * @author: Alvaro Bermejo
 * @author: Francisco Lozano
 * @version: 03/03/2015
 * @since: Assignment 3
 */

public class InvalidMove extends Exception {
	
	//Constructors
	
	/** No-argument constructor.*/
	public InvalidMove() {

	}

	/** Constructor with a detail-message parameter.*/
	public InvalidMove(String msg) {
		super(msg);
	}

	/** Constructor with a detail-message parameter and a cause parameter.*/
	public InvalidMove(String msg, Throwable cause) {
		super(msg);
		cause.printStackTrace();
	}

	/** Constructor with a cause parameter..*/
	public InvalidMove(Throwable cause) {
		cause.printStackTrace();
	}
}